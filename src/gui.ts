import { BrushShape } from "types";
import { Config } from "config";
import * as brush from "brush";

export function createGui(plugin: Plugin, config: Config, stopHandler: () => void): DockWidgetPluginGui {
	const widget = plugin.CreateDockWidgetPluginGui(
		"SprinkleGUI",
		new DockWidgetPluginGuiInfo(Enum.InitialDockState.Left, false, false, 300, 350, 300),
	);

	const BG_COLOR = settings().Studio.Theme.GetColor(Enum.StudioStyleGuideColor.MainBackground);
	const INPUT_BG_COLOR = settings().Studio.Theme.GetColor(Enum.StudioStyleGuideColor.InputFieldBackground);
	const TEXT_COlOR = settings().Studio.Theme.GetColor(Enum.StudioStyleGuideColor.MainText);
	const RED = Color3.fromRGB(179, 58, 58);

	const LEFT_MARGIN = 20;
	const TOP_MARGIN = 20;
	const INTER_ROW_MARGIN = 15;
	const INTER_COL_MARGIN = 30;
	const ROW_SIZE_Y = 25;
	const LABEL_SIZE_X = 150;
	const INPUT_SIZE_X = 80;
	const SHAPE_BUTTON_SIZE = 35;

	const frame = new Instance("ScrollingFrame");
	frame.BackgroundColor3 = BG_COLOR;
	frame.Size = new UDim2(1, 0, 1, 0);
	frame.Parent = widget;

	const list = new Instance("UIListLayout");
	list.Padding = new UDim(0, INTER_ROW_MARGIN);
	list.Parent = frame;

	const stopButtonFrame = new Instance("Frame");
	stopButtonFrame.BackgroundTransparency = 1;
	stopButtonFrame.Size = new UDim2(1, 0, 0, TOP_MARGIN + ROW_SIZE_Y);
	stopButtonFrame.Parent = frame;

	const stopButton = new Instance("TextButton");
	stopButton.Position = new UDim2(0, LEFT_MARGIN, 0, TOP_MARGIN);
	stopButton.Size = new UDim2(0, LABEL_SIZE_X + INTER_COL_MARGIN + INPUT_SIZE_X, 0, ROW_SIZE_Y);
	stopButton.BackgroundColor3 = RED;
	stopButton.Text = "Stop Sprinkling";
	stopButton.TextSize = 16;
	stopButton.Font = Enum.Font.GothamBold;
	stopButton.TextColor3 = TEXT_COlOR;
	stopButton.Parent = stopButtonFrame;

	stopButton.MouseButton1Click.Connect(stopHandler);

	const shapeFrame = new Instance("Frame");
	shapeFrame.BackgroundTransparency = 1;
	shapeFrame.Size = new UDim2(1, 0, 0, SHAPE_BUTTON_SIZE);
	shapeFrame.Parent = frame;

	// Special label, it doesn't have the same size as other
	const shapeLabel = new Instance("TextLabel");
	shapeLabel.BackgroundTransparency = 1;
	shapeLabel.Position = new UDim2(0, LEFT_MARGIN, 0, 0);
	shapeLabel.Size = new UDim2(0, 80, 1, 0);
	shapeLabel.Text = "Shape";
	shapeLabel.TextColor3 = TEXT_COlOR;
	shapeLabel.TextXAlignment = Enum.TextXAlignment.Left;
	shapeLabel.Parent = shapeFrame;

	interface Shape {
		imageId: number;
		type: BrushShape;
	}

	function createShapeButtons(shapes: Array<Shape>) {
		const buttons: Array<ImageButton> = new Array(shapes.size());

		for (let i = 0; i < shapes.size(); i++) {
			const shape = shapes[i];

			const minX = LEFT_MARGIN + shapeLabel.Size.X.Offset + INTER_COL_MARGIN;
			const maxX = LEFT_MARGIN + LABEL_SIZE_X + INTER_COL_MARGIN + INPUT_SIZE_X;

			const marginSpace = maxX - minX - shapes.size() * SHAPE_BUTTON_SIZE;

			const button = new Instance("ImageButton");
			button.BackgroundColor3 = INPUT_BG_COLOR;
			button.Position = new UDim2(
				0,
				// The number of margins is one less than the number of buttons.
				minX + i * (SHAPE_BUTTON_SIZE + marginSpace / (shapes.size() - 1)),
				0,
				0,
			);
			button.Size = new UDim2(0, SHAPE_BUTTON_SIZE, 1, 0);
			button.Image = "rbxthumb://type=Asset&id=" + shape.imageId + "&w=150&h=150";
			button.Parent = shapeFrame;

			// Hide unselected shapes
			if (shape.type !== config.brushShape) {
				button.ImageTransparency = 0.6;
			}

			button.MouseButton1Click.Connect(() => {
				config.brushShape = shape.type;
				const brushPart = brush.get();

				// Change the shape
				if (brushPart !== undefined) {
					switch (config.brushShape) {
						case "Sphere":
							brushPart.Shape = Enum.PartType.Ball;
							break;
						case "Cube":
							brushPart.Shape = Enum.PartType.Block;
							break;
						case "Cylinder":
							brushPart.Shape = Enum.PartType.Cylinder;
							break;
					}
				}

				// Highlight the right button for the shape
				// and hide the others.
				for (let n = 0; n < shapes.size(); n++) {
					if (shapes[n].type === config.brushShape) {
						buttons[n].ImageTransparency = 0;
					} else {
						buttons[n].ImageTransparency = 0.6;
					}
				}
			});

			buttons[i] = button;
		}
	}

	createShapeButtons([
		{ imageId: 7117270534, type: "Sphere" },
		{ imageId: 7117271284, type: "Cube" },
		{ imageId: 7117271867, type: "Cylinder" },
	]);

	interface Row {
		labelText: string;
		value: number | string | boolean;
		updateValue: (newValue: number | string | boolean) => void;
	}

	function createRows(rows: Array<Row>) {
		const startHeight = TOP_MARGIN + SHAPE_BUTTON_SIZE + INTER_ROW_MARGIN;

		for (let i = 0; i < rows.size(); i++) {
			const row = rows[i];

			const rowFrame = new Instance("Frame");
			rowFrame.BackgroundTransparency = 1;
			rowFrame.Size = new UDim2(1, 0, 0, ROW_SIZE_Y);
			rowFrame.Parent = frame;

			const label = new Instance("TextLabel");
			label.BackgroundTransparency = 1;
			label.Position = new UDim2(0, LEFT_MARGIN, 0, 0);
			label.Size = new UDim2(0, LABEL_SIZE_X, 1, 0);
			label.Text = row.labelText;
			label.TextColor3 = TEXT_COlOR;
			label.TextXAlignment = Enum.TextXAlignment.Left;
			label.Parent = rowFrame;

			switch (typeOf(row.value)) {
				case "string":
				case "number": {
					const text = new Instance("TextBox");
					text.BackgroundColor3 = INPUT_BG_COLOR;
					text.Position = new UDim2(0, LEFT_MARGIN + LABEL_SIZE_X + INTER_COL_MARGIN, 0, 0);
					text.Size = new UDim2(0, INPUT_SIZE_X, 0, ROW_SIZE_Y);
					text.TextColor3 = TEXT_COlOR;
					text.Text = "" + row.value;
					text.Parent = rowFrame;

					text.FocusLost.Connect(() => {
						if (typeIs(row.value, "number")) {
							const newValue = tonumber(text.Text);
							if (newValue !== undefined) {
								row.value = newValue;
								row.updateValue(row.value);
							}
						} else {
							row.value = text.Text;
							row.updateValue(row.value);
						}
					});
					break;
				}
				case "boolean": {
					const button = new Instance("TextButton");
					button.BackgroundColor3 = INPUT_BG_COLOR;
					button.Position = new UDim2(0, LEFT_MARGIN + LABEL_SIZE_X + INTER_COL_MARGIN, 0, 0);
					button.Size = new UDim2(0, INPUT_SIZE_X, 0, ROW_SIZE_Y);
					button.TextColor3 = TEXT_COlOR;
					button.Text = "" + row.value;
					button.Parent = rowFrame;

					button.MouseButton1Click.Connect(() => {
						row.value = !(row.value as boolean);
						button.Text = "" + row.value;

						row.updateValue(row.value);
					});
					break;
				}
			}
		}
	}

	createRows([
		{
			labelText: "Brush Size",
			value: config.brushSize,
			updateValue: (value) => {
				config.brushSize = value as number;
				const brushPart = brush.get();

				if (brushPart !== undefined) {
					brushPart.Size = new Vector3(config.brushSize, config.brushSize, config.brushSize);
				}
			},
		},
		{
			labelText: "Max Angle",
			value: config.maxAngle,
			updateValue: (value) => {
				config.maxAngle = value as number;
			},
		},
		{
			labelText: "Snow Depth (in voxel cells)",
			value: config.depth,
			updateValue: (value) => {
				config.depth = value as number;
			},
		},
		{
			labelText: "Transition Zone (in % of angle)",
			value: config.transitionZone,
			updateValue: (value) => {
				config.transitionZone = value as number;
			},
		},
		{
			labelText: "Smoothing factor",
			value: config.smoothing,
			updateValue: (value) => {
				config.smoothing = value as number;
			},
		},
		{
			labelText: "Max Brush Distance",
			value: config.rayMaxDistance,
			updateValue: (value) => {
				config.rayMaxDistance = value as number;
			},
		},
		{
			labelText: "Enable Debug Mode",
			value: config.inDebugMode,
			updateValue: (value) => {
				config.inDebugMode = value as boolean;
			},
		},
		{
			labelText: "Debug Cooldown Time",
			value: config.debugCooldown,
			updateValue: (value) => {
				config.debugCooldown = value as number;
			},
		},
	]);

	const absoluteSize = list.AbsoluteContentSize;
	frame.CanvasSize = new UDim2(0, absoluteSize.X, 0, absoluteSize.Y);

	return widget;
}
