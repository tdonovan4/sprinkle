import { Workspace, PhysicsService, Debris } from "@rbxts/services";
import { BLACKLIST_GROUP } from "consts";

export function showRegion3AndBrush(region: Region3, brush: Part) {
	const regionPart = new Instance("Part");
	regionPart.Name = "Region3Part";
	regionPart.CastShadow = false;
	regionPart.Locked = true;
	regionPart.Anchored = true;
	regionPart.CanCollide = false;
	regionPart.BrickColor = new BrickColor("Br. yellowish orange");
	regionPart.Material = Enum.Material.Neon;
	regionPart.Size = region.Size;
	regionPart.CFrame = region.CFrame;
	regionPart.Transparency = 0.9;
	regionPart.Parent = Workspace;

	PhysicsService.SetPartCollisionGroup(regionPart, BLACKLIST_GROUP);
	// Delete after 2 seconds
	Debris.AddItem(regionPart, 2);

	const brushPart = new Instance("Part");
	brushPart.Name = "BrushPart";
	brushPart.CastShadow = false;
	brushPart.Locked = true;
	brushPart.Anchored = true;
	brushPart.CanCollide = false;
	brushPart.BrickColor = new BrickColor("Medium blue");
	brushPart.Material = Enum.Material.Neon;
	brushPart.Size = brush.Size;
	brushPart.CFrame = brush.CFrame;
	brushPart.Transparency = 0.8;
	brushPart.Shape = brush.Shape;
	brushPart.Parent = Workspace;

	PhysicsService.SetPartCollisionGroup(brushPart, BLACKLIST_GROUP);
	// Delete after 2 seconds
	Debris.AddItem(brushPart, 2);
}

export function showRay(origin: Vector3, ray: RaycastResult) {
	const originPart = new Instance("Part");
	originPart.Name = "RayOriginPart";
	originPart.CastShadow = false;
	originPart.Locked = true;
	originPart.Anchored = true;
	originPart.CanCollide = false;
	originPart.BrickColor = new BrickColor("Light yellow");
	originPart.Material = Enum.Material.Neon;
	originPart.Size = new Vector3(1, 1, 1);
	originPart.CFrame = new CFrame(origin);
	originPart.Transparency = 0.8;
	originPart.Shape = Enum.PartType.Ball;
	originPart.Parent = Workspace;

	const direction = ray.Position.sub(origin);

	const directionPart = new Instance("Part");
	directionPart.Name = "RayDirectionPart";
	directionPart.CastShadow = false;
	directionPart.Locked = true;
	directionPart.Anchored = true;
	directionPart.CanCollide = false;
	directionPart.BrickColor = new BrickColor("White");
	directionPart.Material = Enum.Material.Neon;
	directionPart.Size = new Vector3(0.1, 0.1, direction.Magnitude);
	directionPart.CFrame = new CFrame(origin.add(direction.div(2)), origin.add(direction));
	directionPart.Transparency = 0.8;
	directionPart.Parent = Workspace;

	const normalPart = new Instance("Part");
	normalPart.Name = "RayNormalPart";
	normalPart.CastShadow = false;
	normalPart.Locked = true;
	normalPart.Anchored = true;
	normalPart.CanCollide = false;
	normalPart.BrickColor = new BrickColor("Fire Yellow");
	normalPart.Material = Enum.Material.Neon;
	normalPart.Size = new Vector3(0.1, 0.1, ray.Normal.Magnitude);
	normalPart.CFrame = new CFrame(ray.Position.add(ray.Normal.div(2)), ray.Position.add(ray.Normal));
	normalPart.Transparency = 0.8;
	normalPart.Parent = Workspace;

	PhysicsService.SetPartCollisionGroup(originPart, BLACKLIST_GROUP);
	PhysicsService.SetPartCollisionGroup(directionPart, BLACKLIST_GROUP);
	PhysicsService.SetPartCollisionGroup(normalPart, BLACKLIST_GROUP);
	// Delete after 2 seconds
	Debris.AddItem(originPart, 2);
	Debris.AddItem(directionPart, 2);
	Debris.AddItem(normalPart, 2);
}
