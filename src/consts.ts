const playerId = game.GetService("StudioService").GetUserId();

export const BLACKLIST_GROUP = playerId + " Sprinkle Blacklist";
export const RAYCAST_GROUP = playerId + " Sprinkle Raycast";

export const RAYCAST_PARAMS = new RaycastParams();
RAYCAST_PARAMS.IgnoreWater = true;
RAYCAST_PARAMS.FilterType = Enum.RaycastFilterType.Blacklist;
RAYCAST_PARAMS.CollisionGroup = RAYCAST_GROUP;

export const TERRAIN_RES = 4;
