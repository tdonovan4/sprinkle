import { BrushShape } from "types";

export interface Config {
	brushShape: BrushShape;
	brushSize: number;
	maxAngle: number;
	depth: number;
	transitionZone: number;
	smoothing: number;
	rayMaxDistance: number;
	inDebugMode: boolean;
	debugCooldown: number;
}

export const defaultConfig: Config = {
	brushShape: "Sphere",
	brushSize: 20,
	maxAngle: 45,
	depth: 3,
	transitionZone: 10,
	smoothing: 0.5,
	rayMaxDistance: 500,
	inDebugMode: false,
	debugCooldown: 2,
};
