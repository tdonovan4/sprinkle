import { PhysicsService } from "@rbxts/services";

import { Config } from "config";
import { BLACKLIST_GROUP } from "consts";

let brush: Part | undefined;

export function create(config: Config): Part {
	brush = new Instance("Part");
	brush.Name = "SprinkleBrushPart";
	brush.CastShadow = false;
	brush.Locked = true;
	brush.Anchored = true;
	brush.CanCollide = false;
	brush.BrickColor = new BrickColor("Lime green");
	brush.Material = Enum.Material.Neon;
	brush.Size = new Vector3(config.brushSize, config.brushSize, config.brushSize);
	// This is so that the cylinder brush is upright.
	brush.Rotation = new Vector3(0, 0, 90);
	switch (config.brushShape) {
		case "Sphere":
			brush.Shape = Enum.PartType.Ball;
			break;
		case "Cube":
			brush.Shape = Enum.PartType.Block;
			break;
		case "Cylinder":
			brush.Shape = Enum.PartType.Cylinder;
			break;
	}
	brush.Transparency = 0.8;

	PhysicsService.SetPartCollisionGroup(brush, BLACKLIST_GROUP);

	return brush;
}

export function get(): Part | undefined {
	return brush;
}

export function destroy() {
	brush?.Destroy();
	brush = undefined;
}
