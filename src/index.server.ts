/// <reference types="@rbxts/types/plugin" />
import { RunService, UserInputService, Workspace, PhysicsService } from "@rbxts/services";
const ChangeHistoryService = game.GetService("ChangeHistoryService");

import { BLACKLIST_GROUP, RAYCAST_GROUP, RAYCAST_PARAMS, TERRAIN_RES } from "consts";
import * as gui from "gui";
import { defaultConfig } from "config";
import * as debug from "debug";
import * as brush from "brush";

const TOOLBAR = plugin.CreateToolbar("Terrain Tools");
const BUTTON = TOOLBAR.CreateButton(
	"Sprinkle",
	"Sprinkle some snow on a terrain.",
	"rbxthumb://type=Asset&id=7113205280&w=150&h=150",
);

// Since we pass this function to the gui, we need to
// declare it before it.
function stopSprinkling() {
	plugin.Deactivate();

	const brushPart = brush.get();
	if (brushPart !== undefined) {
		brushPart.Destroy();
	}
	if (heartbeatEventHandler !== undefined) {
		heartbeatEventHandler.Disconnect();
	}
	if (inputEventHandler !== undefined) {
		inputEventHandler.Disconnect();
	}
	PhysicsService.RemoveCollisionGroup(BLACKLIST_GROUP);
	PhysicsService.RemoveCollisionGroup(RAYCAST_GROUP);
	widget.Enabled = false;

	isSprinkling = false;
}

const config = defaultConfig;
const widget = gui.createGui(plugin, config, stopSprinkling);
let heartbeatEventHandler: RBXScriptConnection | undefined;
let inputEventHandler: RBXScriptConnection | undefined;
let isSprinkling = false;

let lastDebugTime = 0;

// The main part of this plugin.
//
// It calculates the normal of the terrain and adds snow if
// the angle is below the critical angle set in the config.
function sprinkle(brushPart: Part) {
	const brushCenter = brushPart.Position;
	// Define the region covered by the brush
	let startRegion = brushCenter.sub(brushPart.Size.div(2));
	let endRegion = brushCenter.add(brushPart.Size.div(2));
	let region = new Region3(startRegion, endRegion);
	region = region.ExpandToGrid(TERRAIN_RES);
	// Update start and end after expand
	startRegion = region.CFrame.Position.sub(region.Size.div(2));
	endRegion = region.CFrame.Position.add(region.Size.div(2));

	const isDebugOk = os.difftime(os.time(), lastDebugTime) >= config.debugCooldown;
	if (config.inDebugMode && isDebugOk) {
		debug.showRegion3AndBrush(region, brushPart);
		lastDebugTime = os.time();
	}

	const [material, occupancy] = Workspace.Terrain.ReadVoxels(region, TERRAIN_RES);

	const radius = config.brushSize / 2;
	const increments = region.Size.div(material.Size);
	const triangleSide = 2;
	const distFromTriCenter = (math.sqrt(3) / 3) * triangleSide;
	const criticalAngle = math.rad(config.maxAngle);
	// A zone between full snow and no snow
	const transitionZoneStart = criticalAngle * (1 - config.transitionZone / 100);

	// Since rays are casted from the sky, we only work on two dimensions.
	for (let x = 0; x < material.Size.X; x++) {
		// Starting from the top
		for (let z = 0; z < material.Size.Z; z++) {
			// To get the world position
			let relativePos = increments.mul(new Vector3(x + 0.5, 0, z + 0.5));
			let worldPosition = startRegion.add(relativePos);

			if (config.brushShape === "Cylinder" || config.brushShape === "Sphere") {
				// This is to restrict the rays into a circle.
				const relativeToCenter = new Vector3(worldPosition.X, 0, worldPosition.Z).sub(
					new Vector3(brushCenter.X, 0, brushCenter.Z),
				);

				if (relativeToCenter.Magnitude > radius) {
					continue;
				}
			}

			let angle;
			let depthLeft = config.depth;
			for (let y = material.Size.Y - 1; y >= 0; y--) {
				// To get the world position
				relativePos = new Vector3(relativePos.X, increments.Y * (y + 2), relativePos.Z);
				worldPosition = startRegion.add(relativePos);

				if (config.brushShape === "Sphere") {
					// This is to restrict the rays into a sphere.
					const relativeToCenter = worldPosition.sub(brushCenter);

					if (relativeToCenter.Magnitude > radius) {
						continue;
					}
				}

				if (occupancy[x][y][z] > 0) {
					if (angle === undefined) {
						const points = [
							new Vector3(worldPosition.X, worldPosition.Y, worldPosition.Z + distFromTriCenter),
							new Vector3(
								worldPosition.X + triangleSide / 2,
								worldPosition.Y,
								worldPosition.Z - distFromTriCenter / 2,
							),
							new Vector3(
								worldPosition.X - triangleSide / 2,
								worldPosition.Y,
								worldPosition.Z - distFromTriCenter / 2,
							),
						];
						let sumAngle = 0;
						const results: Array<RaycastResult> = [];

						for (let i = 0; i < points.size(); i++) {
							// Raycast time.
							const raycastResult = Workspace.Raycast(
								points[i],
								new Vector3(0, -2 * increments.Y, 0),
								RAYCAST_PARAMS,
							);

							if (raycastResult !== undefined) {
								if (config.inDebugMode && isDebugOk) {
									debug.showRay(points[i], raycastResult);
									lastDebugTime = os.time();
								}

								// They both need to be unit vector so we can ignore the division
								// of |a|*|b|.
								sumAngle += math.acos(raycastResult.Normal.Unit.Dot(new Vector3(0, 1, 0)));

								results.push(raycastResult);
							}
						}
						if (results.size() === 3) {
							// Get the normal of the triangle
							const vecA = results[1].Position.sub(results[0].Position);
							const vecB = results[2].Position.sub(results[0].Position);
							const normal = vecA.Cross(vecB).Unit;
							const normalAngle = math.acos(normal.Dot(new Vector3(0, 1, 0)));

							angle = (sumAngle / 3 + normalAngle) / 2;
						} else if (results.size() > 0) {
							angle = sumAngle / results.size();
						} else {
							break;
						}

						if (config.inDebugMode && isDebugOk && angle !== undefined) {
							print("Angle of the slope: " + math.deg(angle));
						}
					}

					if (angle !== undefined) {
						// All of this plugin culminate to this, time to paint.
						if (angle <= transitionZoneStart) {
							material[x][y][z] = Enum.Material.Snow;
						} else if (angle <= criticalAngle) {
							// In the transition zone

							// Perlin noise will return zero if all cordinates are integers, so we need
							// to add a fractional part. The probably of snow decreases linearly as we
							// approach the critical angle.
							if (
								0.5 + math.noise(worldPosition.X + 0.5, worldPosition.Y + 0.5, worldPosition.Z + 0.5) >=
								(angle - transitionZoneStart) / (criticalAngle - transitionZoneStart)
							) {
								material[x][y][z] = Enum.Material.Snow;
							}
						}

						depthLeft--;
					}
				}

				if (depthLeft === 0) {
					break;
				}
			}
		}
	}

	// This saves a few microseconds
	const sizeX = material.Size.X;
	const sizeY = material.Size.Y;
	const sizeZ = material.Size.Z;

	// We can now fill small holes
	for (let x = 0; x < material.Size.X; x++) {
		for (let z = 0; z < material.Size.Z; z++) {
			let depthLeft = config.depth;
			for (let y = 0; y < material.Size.Y; y++) {
				const currentMaterial = material[x][y][z];
				const currentOccupancy = occupancy[x][y][z];
				let earlyStop = false;

				if (currentOccupancy > 0) {
					if (currentMaterial !== Enum.Material.Snow) {
						let snowOccupancy = 0;
						let nonSnowOccupancy = currentOccupancy;

						// Find neighbours (3d version of this).
						// https://www.royvanrijn.com/blog/2019/01/longest-path/
						for (let direction = 0; direction < 27; direction++) {
							if (direction === 13) continue;

							const nRow = x + (direction % 3) - 1;
							const nCol = y + math.floor((direction % 9) / 3 - 1);
							const nDepth = z + math.floor(direction / 9 - 1);

							if (
								nRow < 0 ||
								nRow >= sizeX ||
								nCol < 0 ||
								nCol >= sizeY ||
								nDepth < 0 ||
								nDepth >= sizeZ
							) {
								// We only do smoothing if all neighbours are available
								earlyStop = true;
								break;
							} else {
								if (material[nRow][nCol][nDepth] === Enum.Material.Snow) {
									snowOccupancy += occupancy[nRow][nCol][nDepth];
								} else {
									nonSnowOccupancy += occupancy[nRow][nCol][nDepth];
								}
							}
						}

						if (!earlyStop && nonSnowOccupancy / snowOccupancy <= config.smoothing) {
							material[x][y][z] = Enum.Material.Snow;
						}
					}
					depthLeft--;
				}

				if (depthLeft === 0) {
					break;
				}
			}
		}
	}

	Workspace.Terrain.WriteVoxels(region, TERRAIN_RES, material, occupancy);
}

function startSprinkling() {
	isSprinkling = true;

	widget.Enabled = true;

	// To make sure they don't already exist
	PhysicsService.RemoveCollisionGroup(BLACKLIST_GROUP);
	PhysicsService.RemoveCollisionGroup(RAYCAST_GROUP);

	PhysicsService.CreateCollisionGroup(BLACKLIST_GROUP);
	PhysicsService.CreateCollisionGroup(RAYCAST_GROUP);
	PhysicsService.CollisionGroupSetCollidable(BLACKLIST_GROUP, RAYCAST_GROUP, false);

	const brushPart = brush.create(config);

	heartbeatEventHandler = RunService.Heartbeat.Connect((_) => {
		const mousePos = UserInputService.GetMouseLocation();
		const camera = Workspace.CurrentCamera;

		if (camera !== undefined && brush !== undefined) {
			const unitRay = camera.ScreenPointToRay(mousePos.X, mousePos.Y);
			const raycastResult = Workspace.Raycast(
				unitRay.Origin,
				unitRay.Direction.mul(config.rayMaxDistance),
				RAYCAST_PARAMS,
			);

			if (raycastResult !== undefined && raycastResult.Instance.ClassName === "Terrain") {
				if (brushPart.Parent === undefined) {
					brushPart.Parent = Workspace;
				}

				brushPart.Position = raycastResult.Position;

				if (UserInputService.IsMouseButtonPressed(Enum.UserInputType.MouseButton1)) {
					sprinkle(brushPart);
				}
			} else {
				brushPart.Parent = undefined;
			}
		}
	});

	inputEventHandler = UserInputService.InputEnded.Connect((input, _gameProcessed) => {
		if (input.KeyCode === Enum.KeyCode.Escape) {
			stopSprinkling();
		}

		if (input.UserInputType === Enum.UserInputType.MouseButton1) {
			if (brushPart.Parent !== undefined) {
				ChangeHistoryService.SetWaypoint("Sprinkle made a change");
			}
		}
	});

	// This removes the selection box when dragging the mouse.
	plugin.Activate(false);

	// Setup is done, set waypoint
	ChangeHistoryService.SetWaypoint("Sprinkle setup done");
}

BUTTON.Click.Connect(() => {
	if (!isSprinkling) {
		startSprinkling();
	}
});

// Cleanup tasks
plugin.Unloading.Connect(() => {
	stopSprinkling();
});
